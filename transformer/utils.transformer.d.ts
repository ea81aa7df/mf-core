export declare const trim: {
    from(value: string): string;
    to(value: string): string;
};
export declare const trimAndTitle: {
    from(value: string): string;
    to(value: string): string;
};
export declare const stringToLowerCase: {
    from(value: string): string;
    to(value: string): string;
};
//# sourceMappingURL=utils.transformer.d.ts.map