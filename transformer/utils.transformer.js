"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.stringToLowerCase = exports.trimAndTitle = exports.trim = void 0;
const lodash_1 = require("lodash");
exports.trim = {
    from(value) {
        return value;
    },
    to(value) {
        if (typeof value === 'string') {
            return value.trim();
        }
        return value;
    },
};
exports.trimAndTitle = {
    from(value) {
        return value;
    },
    to(value) {
        if (typeof value === 'string') {
            return (0, lodash_1.startCase)(value).trim();
        }
        return value;
    },
};
exports.stringToLowerCase = {
    from(value) {
        return value;
    },
    to(value) {
        if (value && typeof value === 'string') {
            return value.toLowerCase();
        }
        return value;
    },
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidXRpbHMudHJhbnNmb3JtZXIuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi9zcmMvdHJhbnNmb3JtZXIvdXRpbHMudHJhbnNmb3JtZXIudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7O0FBQUEsbUNBQW1DO0FBRXRCLFFBQUEsSUFBSSxHQUFHO0lBQ2xCLElBQUksQ0FBQyxLQUFhO1FBQ2hCLE9BQU8sS0FBSyxDQUFDO0lBQ2YsQ0FBQztJQUNELEVBQUUsQ0FBQyxLQUFhO1FBQ2QsSUFBSSxPQUFPLEtBQUssS0FBSyxRQUFRLEVBQUU7WUFDN0IsT0FBTyxLQUFLLENBQUMsSUFBSSxFQUFFLENBQUM7U0FDckI7UUFFRCxPQUFPLEtBQUssQ0FBQztJQUNmLENBQUM7Q0FDRixDQUFDO0FBRVcsUUFBQSxZQUFZLEdBQUc7SUFDMUIsSUFBSSxDQUFDLEtBQWE7UUFDaEIsT0FBTyxLQUFLLENBQUM7SUFDZixDQUFDO0lBQ0QsRUFBRSxDQUFDLEtBQWE7UUFDZCxJQUFJLE9BQU8sS0FBSyxLQUFLLFFBQVEsRUFBRTtZQUM3QixPQUFPLElBQUEsa0JBQVMsRUFBQyxLQUFLLENBQUMsQ0FBQyxJQUFJLEVBQUUsQ0FBQztTQUNoQztRQUVELE9BQU8sS0FBSyxDQUFDO0lBQ2YsQ0FBQztDQUNGLENBQUM7QUFFVyxRQUFBLGlCQUFpQixHQUFHO0lBQy9CLElBQUksQ0FBQyxLQUFhO1FBQ2hCLE9BQU8sS0FBSyxDQUFDO0lBQ2YsQ0FBQztJQUNELEVBQUUsQ0FBQyxLQUFhO1FBQ2QsSUFBSSxLQUFLLElBQUksT0FBTyxLQUFLLEtBQUssUUFBUSxFQUFFO1lBQ3RDLE9BQU8sS0FBSyxDQUFDLFdBQVcsRUFBRSxDQUFDO1NBQzVCO1FBRUQsT0FBTyxLQUFLLENBQUM7SUFDZixDQUFDO0NBQ0YsQ0FBQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IHN0YXJ0Q2FzZSB9IGZyb20gJ2xvZGFzaCc7XG5cbmV4cG9ydCBjb25zdCB0cmltID0ge1xuICBmcm9tKHZhbHVlOiBzdHJpbmcpIHtcbiAgICByZXR1cm4gdmFsdWU7XG4gIH0sXG4gIHRvKHZhbHVlOiBzdHJpbmcpIHtcbiAgICBpZiAodHlwZW9mIHZhbHVlID09PSAnc3RyaW5nJykge1xuICAgICAgcmV0dXJuIHZhbHVlLnRyaW0oKTtcbiAgICB9XG5cbiAgICByZXR1cm4gdmFsdWU7XG4gIH0sXG59O1xuXG5leHBvcnQgY29uc3QgdHJpbUFuZFRpdGxlID0ge1xuICBmcm9tKHZhbHVlOiBzdHJpbmcpIHtcbiAgICByZXR1cm4gdmFsdWU7XG4gIH0sXG4gIHRvKHZhbHVlOiBzdHJpbmcpIHtcbiAgICBpZiAodHlwZW9mIHZhbHVlID09PSAnc3RyaW5nJykge1xuICAgICAgcmV0dXJuIHN0YXJ0Q2FzZSh2YWx1ZSkudHJpbSgpO1xuICAgIH1cblxuICAgIHJldHVybiB2YWx1ZTtcbiAgfSxcbn07XG5cbmV4cG9ydCBjb25zdCBzdHJpbmdUb0xvd2VyQ2FzZSA9IHtcbiAgZnJvbSh2YWx1ZTogc3RyaW5nKSB7XG4gICAgcmV0dXJuIHZhbHVlO1xuICB9LFxuICB0byh2YWx1ZTogc3RyaW5nKSB7XG4gICAgaWYgKHZhbHVlICYmIHR5cGVvZiB2YWx1ZSA9PT0gJ3N0cmluZycpIHtcbiAgICAgIHJldHVybiB2YWx1ZS50b0xvd2VyQ2FzZSgpO1xuICAgIH1cblxuICAgIHJldHVybiB2YWx1ZTtcbiAgfSxcbn07XG4iXX0=