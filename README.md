## Project

## Description

This repository ships the core services to most other repositories, such as entities, DTOs, types, services, and so on.

## Installation

```bash
$ npm install
```

## Release

```bash
$ npm run release
```

## Frameworks

[Nest](https://github.com/nestjs/nest) framework TypeScript starter repository.  
[TypeORM](https://github.com/typeorm/typeorm) is an ORM that can run in TypeScript.
