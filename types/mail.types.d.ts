export interface MailSendOptions {
    subject: string;
    text: string;
    to: string;
}
//# sourceMappingURL=mail.types.d.ts.map