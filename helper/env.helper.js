"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.getEnvPath = void 0;
const fs_1 = require("fs");
const path_1 = require("path");
function getEnvPath(dest) {
    const env = process.env.NODE_ENV;
    const fallback = (0, path_1.resolve)(`${dest}/dev.env`);
    let filename = 'development.env';
    if (env !== undefined) {
        filename = `${env}.env`;
    }
    let filePath = (0, path_1.resolve)(`${dest}/${filename}`);
    if (!(0, fs_1.existsSync)(filePath)) {
        filePath = fallback;
    }
    return filePath;
}
exports.getEnvPath = getEnvPath;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZW52LmhlbHBlci5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uL3NyYy9oZWxwZXIvZW52LmhlbHBlci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7QUFBQSwyQkFBZ0M7QUFDaEMsK0JBQStCO0FBRS9CLFNBQWdCLFVBQVUsQ0FBQyxJQUFZO0lBQ3JDLE1BQU0sR0FBRyxHQUF1QixPQUFPLENBQUMsR0FBRyxDQUFDLFFBQVEsQ0FBQztJQUNyRCxNQUFNLFFBQVEsR0FBVyxJQUFBLGNBQU8sRUFBQyxHQUFHLElBQUksVUFBVSxDQUFDLENBQUM7SUFDcEQsSUFBSSxRQUFRLEdBQVcsaUJBQWlCLENBQUM7SUFFekMsSUFBSSxHQUFHLEtBQUssU0FBUyxFQUFFO1FBQ3JCLFFBQVEsR0FBRyxHQUFHLEdBQUcsTUFBTSxDQUFDO0tBQ3pCO0lBRUQsSUFBSSxRQUFRLEdBQVcsSUFBQSxjQUFPLEVBQUMsR0FBRyxJQUFJLElBQUksUUFBUSxFQUFFLENBQUMsQ0FBQztJQUV0RCxJQUFJLENBQUMsSUFBQSxlQUFVLEVBQUMsUUFBUSxDQUFDLEVBQUU7UUFDekIsUUFBUSxHQUFHLFFBQVEsQ0FBQztLQUNyQjtJQUVELE9BQU8sUUFBUSxDQUFDO0FBQ2xCLENBQUM7QUFoQkQsZ0NBZ0JDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgZXhpc3RzU3luYyB9IGZyb20gJ2ZzJztcbmltcG9ydCB7IHJlc29sdmUgfSBmcm9tICdwYXRoJztcblxuZXhwb3J0IGZ1bmN0aW9uIGdldEVudlBhdGgoZGVzdDogc3RyaW5nKTogc3RyaW5nIHtcbiAgY29uc3QgZW52OiBzdHJpbmcgfCB1bmRlZmluZWQgPSBwcm9jZXNzLmVudi5OT0RFX0VOVjtcbiAgY29uc3QgZmFsbGJhY2s6IHN0cmluZyA9IHJlc29sdmUoYCR7ZGVzdH0vZGV2LmVudmApO1xuICBsZXQgZmlsZW5hbWU6IHN0cmluZyA9ICdkZXZlbG9wbWVudC5lbnYnO1xuXG4gIGlmIChlbnYgIT09IHVuZGVmaW5lZCkge1xuICAgIGZpbGVuYW1lID0gYCR7ZW52fS5lbnZgO1xuICB9XG5cbiAgbGV0IGZpbGVQYXRoOiBzdHJpbmcgPSByZXNvbHZlKGAke2Rlc3R9LyR7ZmlsZW5hbWV9YCk7XG5cbiAgaWYgKCFleGlzdHNTeW5jKGZpbGVQYXRoKSkge1xuICAgIGZpbGVQYXRoID0gZmFsbGJhY2s7XG4gIH1cblxuICByZXR1cm4gZmlsZVBhdGg7XG59XG4iXX0=