export interface ToNumberOptions {
    default?: number;
    min?: number;
    max?: number;
}
export interface ToDateOptions {
    nullable: boolean;
}
export declare function toBoolean(value: number | string | any): boolean;
export declare function toEnum(value: string, eNuM: any): any;
export declare function toNumber(value: string, opts: ToNumberOptions): number;
export declare function toNumberArray(value: string[]): number[];
export declare function toDate(value: string, opts: ToDateOptions): Date | null;
//# sourceMappingURL=cast.helper.d.ts.map