export interface MailSendOptions {
  subject: string;
  text: string;
  to: string;
}
