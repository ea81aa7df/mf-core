import { startCase } from 'lodash';

export const trim = {
  from(value: string) {
    return value;
  },
  to(value: string) {
    if (typeof value === 'string') {
      return value.trim();
    }

    return value;
  },
};

export const trimAndTitle = {
  from(value: string) {
    return value;
  },
  to(value: string) {
    if (typeof value === 'string') {
      return startCase(value).trim();
    }

    return value;
  },
};

export const stringToLowerCase = {
  from(value: string) {
    return value;
  },
  to(value: string) {
    if (value && typeof value === 'string') {
      return value.toLowerCase();
    }

    return value;
  },
};
