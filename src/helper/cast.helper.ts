export interface ToNumberOptions {
  default?: number;
  min?: number;
  max?: number;
}

export interface ToDateOptions {
  nullable: boolean;
}

export function toBoolean(value: number | string | any): boolean {
  if (typeof value === 'boolean') {
    return value;
  } else if (typeof value === 'number') {
    if (!value || value <= 0) {
      return false;
    } else {
      return true;
    }
  } else if (typeof value === 'string') {
    value = value.toLowerCase();

    if (value === 'true' || value === '1') {
      return true;
    } else {
      return false;
    }
  }

  return false;
}

export function toEnum(value: string, eNuM: any): any {
  return Object.values(eNuM).includes(value) ? value : eNuM[Object.keys(eNuM)[0]];
}

export function toNumber(value: string, opts: ToNumberOptions): number {
  let newValue: number = Number.parseInt(value || String(opts.default), 10);

  if (Number.isNaN(newValue)) {
    newValue = opts.default;
  }

  if (opts && typeof opts.min === 'number') {
    if (newValue < opts.min) {
      newValue = opts.min;
    }
  }

  if (opts && typeof opts.min === 'number') {
    if (newValue > opts.max) {
      newValue = opts.max;
    }
  }

  return newValue;
}

export function toNumberArray(value: string[]): number[] {
  if (typeof value !== 'object' || !Array.isArray(value)) {
    return;
  }

  const newValue: number[] = [];

  for (let index = 0; index < value.length; index++) {
    const item: string = String(value[index]);

    if (item === '') {
      continue;
    }

    newValue.push(Number.parseInt(item || '0', 10));
  }

  return newValue;
}

export function toDate(value: string, opts: ToDateOptions): Date | null {
  if (!value && opts && opts.nullable) {
    return null;
  } else if (!value) {
    return new Date();
  }

  let newValue: Date = new Date(value);

  return newValue;
}
