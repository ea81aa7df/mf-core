import { Entity, PrimaryGeneratedColumn, Column, CreateDateColumn, UpdateDateColumn, OneToMany } from 'typeorm';
import { User } from '..';

@Entity()
export class Organisation {
  @PrimaryGeneratedColumn()
  public id!: number;

  @Column({ type: 'boolean', default: false })
  public isDeleted: boolean;

  /*
   * Create and Update Date Columns
   */

  @CreateDateColumn({ type: 'timestamp' })
  public createdAt!: Date;

  @UpdateDateColumn({ type: 'timestamp' })
  public updatedAt!: Date;

  /*
   * One To Many Relations
   */

  @OneToMany(() => User, (user) => user.organisation)
  public users: User[];
}
