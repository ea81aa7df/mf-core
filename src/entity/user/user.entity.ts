import { Exclude } from 'class-transformer';
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  OneToMany,
  JoinColumn,
  ManyToOne,
} from 'typeorm';
import { stringToLowerCase } from '../../transformer/utils.transformer';
import { Organisation } from '../organisation/organisation.entity';
import { UserApp } from './app.entity';

@Entity()
export class User {
  @PrimaryGeneratedColumn()
  public id!: number;

  @Column({ type: 'varchar', length: 120, transformer: stringToLowerCase })
  public email: string;

  @Exclude()
  @Column({ type: 'varchar', length: 120 })
  public password: string;

  @Column({ type: 'varchar', length: 120, nullable: true })
  public code: string | null;

  @Column({ type: 'boolean', default: false })
  public isActive: boolean;

  @Column({ type: 'boolean', default: false })
  public isDeleted: boolean;

  @Column({ type: 'timestamp', nullable: true, default: null })
  public lastLoginAt: Date | null;

  /*
   * Create and Update Date Columns
   */

  @CreateDateColumn({ type: 'timestamp' })
  public createdAt!: Date;

  @UpdateDateColumn({ type: 'timestamp' })
  public updatedAt!: Date;

  /*
   * Many To One Relations
   */

  @ManyToOne(() => Organisation, (organisation) => organisation.users, { nullable: false })
  @JoinColumn()
  public organisation: Organisation;

  /*
   * One To Many Relations
   */

  @OneToMany(() => UserApp, (userApp) => userApp.user)
  public apps: UserApp[];
}
