import { Entity, CreateDateColumn, UpdateDateColumn, ManyToOne, PrimaryGeneratedColumn, JoinColumn, Column } from 'typeorm';
import { App, User } from '..';

@Entity()
export class UserApp {
  @PrimaryGeneratedColumn()
  public id!: number;

  @Column({ type: 'json', nullable: true })
  public credentials: any | null;

  @Column({ type: 'varchar', nullable: true })
  public email: string | null;

  @Column({ type: 'boolean', default: false })
  public isActive: boolean;

  @Column({ type: 'boolean', default: false })
  public isDeleted: boolean;

  @ManyToOne(() => App, (app) => app.users)
  @JoinColumn()
  public app: App;

  @ManyToOne(() => User, (user) => user.apps)
  @JoinColumn()
  public user: User;

  @CreateDateColumn({ type: 'timestamp' })
  public createdAt: Date;

  @UpdateDateColumn({ type: 'timestamp' })
  public updatedAt: Date;
}
