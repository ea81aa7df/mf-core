import { Entity, PrimaryGeneratedColumn, Column, CreateDateColumn, UpdateDateColumn, OneToMany } from 'typeorm';
import { UserApp } from '../user/app.entity';

@Entity()
export class App {
  @PrimaryGeneratedColumn()
  public id!: number;

  @Column({ type: 'varchar', length: 120 })
  public name: string;

  @Column({ type: 'boolean', default: true })
  public isActive: boolean;

  /*
   * Create and Update Date Columns
   */

  @CreateDateColumn({ type: 'timestamp' })
  public createdAt!: Date;

  @UpdateDateColumn({ type: 'timestamp' })
  public updatedAt!: Date;

  /*
   * One To Many Relations
   */

  @OneToMany(() => UserApp, (userApp) => userApp.app)
  public users: UserApp[];

  /*
   * Constructor
   */

  constructor(partial?: Partial<App>) {
    if (!partial) {
      return;
    }

    Object.assign(this, partial);
  }
}
