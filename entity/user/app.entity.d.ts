import { App, User } from '..';
export declare class UserApp {
    id: number;
    credentials: any | null;
    email: string | null;
    isActive: boolean;
    isDeleted: boolean;
    app: App;
    user: User;
    createdAt: Date;
    updatedAt: Date;
}
//# sourceMappingURL=app.entity.d.ts.map