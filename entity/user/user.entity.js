"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.User = void 0;
const class_transformer_1 = require("class-transformer");
const typeorm_1 = require("typeorm");
const utils_transformer_1 = require("../../transformer/utils.transformer");
const organisation_entity_1 = require("../organisation/organisation.entity");
const app_entity_1 = require("./app.entity");
let User = class User {
};
__decorate([
    (0, typeorm_1.PrimaryGeneratedColumn)(),
    __metadata("design:type", Number)
], User.prototype, "id", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: 'varchar', length: 120, transformer: utils_transformer_1.stringToLowerCase }),
    __metadata("design:type", String)
], User.prototype, "email", void 0);
__decorate([
    (0, class_transformer_1.Exclude)(),
    (0, typeorm_1.Column)({ type: 'varchar', length: 120 }),
    __metadata("design:type", String)
], User.prototype, "password", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: 'varchar', length: 120, nullable: true }),
    __metadata("design:type", String)
], User.prototype, "code", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: 'boolean', default: false }),
    __metadata("design:type", Boolean)
], User.prototype, "isActive", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: 'boolean', default: false }),
    __metadata("design:type", Boolean)
], User.prototype, "isDeleted", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: 'timestamp', nullable: true, default: null }),
    __metadata("design:type", Date)
], User.prototype, "lastLoginAt", void 0);
__decorate([
    (0, typeorm_1.CreateDateColumn)({ type: 'timestamp' }),
    __metadata("design:type", Date)
], User.prototype, "createdAt", void 0);
__decorate([
    (0, typeorm_1.UpdateDateColumn)({ type: 'timestamp' }),
    __metadata("design:type", Date)
], User.prototype, "updatedAt", void 0);
__decorate([
    (0, typeorm_1.ManyToOne)(() => organisation_entity_1.Organisation, (organisation) => organisation.users, { nullable: false }),
    (0, typeorm_1.JoinColumn)(),
    __metadata("design:type", organisation_entity_1.Organisation)
], User.prototype, "organisation", void 0);
__decorate([
    (0, typeorm_1.OneToMany)(() => app_entity_1.UserApp, (userApp) => userApp.user),
    __metadata("design:type", Array)
], User.prototype, "apps", void 0);
User = __decorate([
    (0, typeorm_1.Entity)()
], User);
exports.User = User;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidXNlci5lbnRpdHkuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi9zcmMvZW50aXR5L3VzZXIvdXNlci5lbnRpdHkudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7O0FBQUEseURBQTRDO0FBQzVDLHFDQVNpQjtBQUNqQiwyRUFBd0U7QUFDeEUsNkVBQW1FO0FBQ25FLDZDQUF1QztBQUd2QyxJQUFhLElBQUksR0FBakIsTUFBYSxJQUFJO0NBK0NoQixDQUFBO0FBN0NDO0lBREMsSUFBQSxnQ0FBc0IsR0FBRTs7Z0NBQ047QUFHbkI7SUFEQyxJQUFBLGdCQUFNLEVBQUMsRUFBRSxJQUFJLEVBQUUsU0FBUyxFQUFFLE1BQU0sRUFBRSxHQUFHLEVBQUUsV0FBVyxFQUFFLHFDQUFpQixFQUFFLENBQUM7O21DQUNwRDtBQUlyQjtJQUZDLElBQUEsMkJBQU8sR0FBRTtJQUNULElBQUEsZ0JBQU0sRUFBQyxFQUFFLElBQUksRUFBRSxTQUFTLEVBQUUsTUFBTSxFQUFFLEdBQUcsRUFBRSxDQUFDOztzQ0FDakI7QUFHeEI7SUFEQyxJQUFBLGdCQUFNLEVBQUMsRUFBRSxJQUFJLEVBQUUsU0FBUyxFQUFFLE1BQU0sRUFBRSxHQUFHLEVBQUUsUUFBUSxFQUFFLElBQUksRUFBRSxDQUFDOztrQ0FDOUI7QUFHM0I7SUFEQyxJQUFBLGdCQUFNLEVBQUMsRUFBRSxJQUFJLEVBQUUsU0FBUyxFQUFFLE9BQU8sRUFBRSxLQUFLLEVBQUUsQ0FBQzs7c0NBQ25CO0FBR3pCO0lBREMsSUFBQSxnQkFBTSxFQUFDLEVBQUUsSUFBSSxFQUFFLFNBQVMsRUFBRSxPQUFPLEVBQUUsS0FBSyxFQUFFLENBQUM7O3VDQUNsQjtBQUcxQjtJQURDLElBQUEsZ0JBQU0sRUFBQyxFQUFFLElBQUksRUFBRSxXQUFXLEVBQUUsUUFBUSxFQUFFLElBQUksRUFBRSxPQUFPLEVBQUUsSUFBSSxFQUFFLENBQUM7OEJBQ3pDLElBQUk7eUNBQVE7QUFPaEM7SUFEQyxJQUFBLDBCQUFnQixFQUFDLEVBQUUsSUFBSSxFQUFFLFdBQVcsRUFBRSxDQUFDOzhCQUNyQixJQUFJO3VDQUFDO0FBR3hCO0lBREMsSUFBQSwwQkFBZ0IsRUFBQyxFQUFFLElBQUksRUFBRSxXQUFXLEVBQUUsQ0FBQzs4QkFDckIsSUFBSTt1Q0FBQztBQVF4QjtJQUZDLElBQUEsbUJBQVMsRUFBQyxHQUFHLEVBQUUsQ0FBQyxrQ0FBWSxFQUFFLENBQUMsWUFBWSxFQUFFLEVBQUUsQ0FBQyxZQUFZLENBQUMsS0FBSyxFQUFFLEVBQUUsUUFBUSxFQUFFLEtBQUssRUFBRSxDQUFDO0lBQ3hGLElBQUEsb0JBQVUsR0FBRTs4QkFDUSxrQ0FBWTswQ0FBQztBQU9sQztJQURDLElBQUEsbUJBQVMsRUFBQyxHQUFHLEVBQUUsQ0FBQyxvQkFBTyxFQUFFLENBQUMsT0FBTyxFQUFFLEVBQUUsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDOztrQ0FDN0I7QUE5Q1osSUFBSTtJQURoQixJQUFBLGdCQUFNLEdBQUU7R0FDSSxJQUFJLENBK0NoQjtBQS9DWSxvQkFBSSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEV4Y2x1ZGUgfSBmcm9tICdjbGFzcy10cmFuc2Zvcm1lcic7XG5pbXBvcnQge1xuICBFbnRpdHksXG4gIFByaW1hcnlHZW5lcmF0ZWRDb2x1bW4sXG4gIENvbHVtbixcbiAgQ3JlYXRlRGF0ZUNvbHVtbixcbiAgVXBkYXRlRGF0ZUNvbHVtbixcbiAgT25lVG9NYW55LFxuICBKb2luQ29sdW1uLFxuICBNYW55VG9PbmUsXG59IGZyb20gJ3R5cGVvcm0nO1xuaW1wb3J0IHsgc3RyaW5nVG9Mb3dlckNhc2UgfSBmcm9tICcuLi8uLi90cmFuc2Zvcm1lci91dGlscy50cmFuc2Zvcm1lcic7XG5pbXBvcnQgeyBPcmdhbmlzYXRpb24gfSBmcm9tICcuLi9vcmdhbmlzYXRpb24vb3JnYW5pc2F0aW9uLmVudGl0eSc7XG5pbXBvcnQgeyBVc2VyQXBwIH0gZnJvbSAnLi9hcHAuZW50aXR5JztcblxuQEVudGl0eSgpXG5leHBvcnQgY2xhc3MgVXNlciB7XG4gIEBQcmltYXJ5R2VuZXJhdGVkQ29sdW1uKClcbiAgcHVibGljIGlkITogbnVtYmVyO1xuXG4gIEBDb2x1bW4oeyB0eXBlOiAndmFyY2hhcicsIGxlbmd0aDogMTIwLCB0cmFuc2Zvcm1lcjogc3RyaW5nVG9Mb3dlckNhc2UgfSlcbiAgcHVibGljIGVtYWlsOiBzdHJpbmc7XG5cbiAgQEV4Y2x1ZGUoKVxuICBAQ29sdW1uKHsgdHlwZTogJ3ZhcmNoYXInLCBsZW5ndGg6IDEyMCB9KVxuICBwdWJsaWMgcGFzc3dvcmQ6IHN0cmluZztcblxuICBAQ29sdW1uKHsgdHlwZTogJ3ZhcmNoYXInLCBsZW5ndGg6IDEyMCwgbnVsbGFibGU6IHRydWUgfSlcbiAgcHVibGljIGNvZGU6IHN0cmluZyB8IG51bGw7XG5cbiAgQENvbHVtbih7IHR5cGU6ICdib29sZWFuJywgZGVmYXVsdDogZmFsc2UgfSlcbiAgcHVibGljIGlzQWN0aXZlOiBib29sZWFuO1xuXG4gIEBDb2x1bW4oeyB0eXBlOiAnYm9vbGVhbicsIGRlZmF1bHQ6IGZhbHNlIH0pXG4gIHB1YmxpYyBpc0RlbGV0ZWQ6IGJvb2xlYW47XG5cbiAgQENvbHVtbih7IHR5cGU6ICd0aW1lc3RhbXAnLCBudWxsYWJsZTogdHJ1ZSwgZGVmYXVsdDogbnVsbCB9KVxuICBwdWJsaWMgbGFzdExvZ2luQXQ6IERhdGUgfCBudWxsO1xuXG4gIC8qXG4gICAqIENyZWF0ZSBhbmQgVXBkYXRlIERhdGUgQ29sdW1uc1xuICAgKi9cblxuICBAQ3JlYXRlRGF0ZUNvbHVtbih7IHR5cGU6ICd0aW1lc3RhbXAnIH0pXG4gIHB1YmxpYyBjcmVhdGVkQXQhOiBEYXRlO1xuXG4gIEBVcGRhdGVEYXRlQ29sdW1uKHsgdHlwZTogJ3RpbWVzdGFtcCcgfSlcbiAgcHVibGljIHVwZGF0ZWRBdCE6IERhdGU7XG5cbiAgLypcbiAgICogTWFueSBUbyBPbmUgUmVsYXRpb25zXG4gICAqL1xuXG4gIEBNYW55VG9PbmUoKCkgPT4gT3JnYW5pc2F0aW9uLCAob3JnYW5pc2F0aW9uKSA9PiBvcmdhbmlzYXRpb24udXNlcnMsIHsgbnVsbGFibGU6IGZhbHNlIH0pXG4gIEBKb2luQ29sdW1uKClcbiAgcHVibGljIG9yZ2FuaXNhdGlvbjogT3JnYW5pc2F0aW9uO1xuXG4gIC8qXG4gICAqIE9uZSBUbyBNYW55IFJlbGF0aW9uc1xuICAgKi9cblxuICBAT25lVG9NYW55KCgpID0+IFVzZXJBcHAsICh1c2VyQXBwKSA9PiB1c2VyQXBwLnVzZXIpXG4gIHB1YmxpYyBhcHBzOiBVc2VyQXBwW107XG59XG4iXX0=