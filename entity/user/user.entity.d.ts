import { Organisation } from '../organisation/organisation.entity';
import { UserApp } from './app.entity';
export declare class User {
    id: number;
    email: string;
    password: string;
    code: string | null;
    isActive: boolean;
    isDeleted: boolean;
    lastLoginAt: Date | null;
    createdAt: Date;
    updatedAt: Date;
    organisation: Organisation;
    apps: UserApp[];
}
//# sourceMappingURL=user.entity.d.ts.map