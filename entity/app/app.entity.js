"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.App = void 0;
const typeorm_1 = require("typeorm");
const app_entity_1 = require("../user/app.entity");
let App = class App {
    constructor(partial) {
        if (!partial) {
            return;
        }
        Object.assign(this, partial);
    }
};
__decorate([
    (0, typeorm_1.PrimaryGeneratedColumn)(),
    __metadata("design:type", Number)
], App.prototype, "id", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: 'varchar', length: 120 }),
    __metadata("design:type", String)
], App.prototype, "name", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: 'boolean', default: true }),
    __metadata("design:type", Boolean)
], App.prototype, "isActive", void 0);
__decorate([
    (0, typeorm_1.CreateDateColumn)({ type: 'timestamp' }),
    __metadata("design:type", Date)
], App.prototype, "createdAt", void 0);
__decorate([
    (0, typeorm_1.UpdateDateColumn)({ type: 'timestamp' }),
    __metadata("design:type", Date)
], App.prototype, "updatedAt", void 0);
__decorate([
    (0, typeorm_1.OneToMany)(() => app_entity_1.UserApp, (userApp) => userApp.app),
    __metadata("design:type", Array)
], App.prototype, "users", void 0);
App = __decorate([
    (0, typeorm_1.Entity)(),
    __metadata("design:paramtypes", [Object])
], App);
exports.App = App;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXBwLmVudGl0eS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy9lbnRpdHkvYXBwL2FwcC5lbnRpdHkudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7O0FBQUEscUNBQWdIO0FBQ2hILG1EQUE2QztBQUc3QyxJQUFhLEdBQUcsR0FBaEIsTUFBYSxHQUFHO0lBK0JkLFlBQVksT0FBc0I7UUFDaEMsSUFBSSxDQUFDLE9BQU8sRUFBRTtZQUNaLE9BQU87U0FDUjtRQUVELE1BQU0sQ0FBQyxNQUFNLENBQUMsSUFBSSxFQUFFLE9BQU8sQ0FBQyxDQUFDO0lBQy9CLENBQUM7Q0FDRixDQUFBO0FBcENDO0lBREMsSUFBQSxnQ0FBc0IsR0FBRTs7K0JBQ047QUFHbkI7SUFEQyxJQUFBLGdCQUFNLEVBQUMsRUFBRSxJQUFJLEVBQUUsU0FBUyxFQUFFLE1BQU0sRUFBRSxHQUFHLEVBQUUsQ0FBQzs7aUNBQ3JCO0FBR3BCO0lBREMsSUFBQSxnQkFBTSxFQUFDLEVBQUUsSUFBSSxFQUFFLFNBQVMsRUFBRSxPQUFPLEVBQUUsSUFBSSxFQUFFLENBQUM7O3FDQUNsQjtBQU96QjtJQURDLElBQUEsMEJBQWdCLEVBQUMsRUFBRSxJQUFJLEVBQUUsV0FBVyxFQUFFLENBQUM7OEJBQ3JCLElBQUk7c0NBQUM7QUFHeEI7SUFEQyxJQUFBLDBCQUFnQixFQUFDLEVBQUUsSUFBSSxFQUFFLFdBQVcsRUFBRSxDQUFDOzhCQUNyQixJQUFJO3NDQUFDO0FBT3hCO0lBREMsSUFBQSxtQkFBUyxFQUFDLEdBQUcsRUFBRSxDQUFDLG9CQUFPLEVBQUUsQ0FBQyxPQUFPLEVBQUUsRUFBRSxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUM7O2tDQUMzQjtBQXpCYixHQUFHO0lBRGYsSUFBQSxnQkFBTSxHQUFFOztHQUNJLEdBQUcsQ0FzQ2Y7QUF0Q1ksa0JBQUciLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBFbnRpdHksIFByaW1hcnlHZW5lcmF0ZWRDb2x1bW4sIENvbHVtbiwgQ3JlYXRlRGF0ZUNvbHVtbiwgVXBkYXRlRGF0ZUNvbHVtbiwgT25lVG9NYW55IH0gZnJvbSAndHlwZW9ybSc7XG5pbXBvcnQgeyBVc2VyQXBwIH0gZnJvbSAnLi4vdXNlci9hcHAuZW50aXR5JztcblxuQEVudGl0eSgpXG5leHBvcnQgY2xhc3MgQXBwIHtcbiAgQFByaW1hcnlHZW5lcmF0ZWRDb2x1bW4oKVxuICBwdWJsaWMgaWQhOiBudW1iZXI7XG5cbiAgQENvbHVtbih7IHR5cGU6ICd2YXJjaGFyJywgbGVuZ3RoOiAxMjAgfSlcbiAgcHVibGljIG5hbWU6IHN0cmluZztcblxuICBAQ29sdW1uKHsgdHlwZTogJ2Jvb2xlYW4nLCBkZWZhdWx0OiB0cnVlIH0pXG4gIHB1YmxpYyBpc0FjdGl2ZTogYm9vbGVhbjtcblxuICAvKlxuICAgKiBDcmVhdGUgYW5kIFVwZGF0ZSBEYXRlIENvbHVtbnNcbiAgICovXG5cbiAgQENyZWF0ZURhdGVDb2x1bW4oeyB0eXBlOiAndGltZXN0YW1wJyB9KVxuICBwdWJsaWMgY3JlYXRlZEF0ITogRGF0ZTtcblxuICBAVXBkYXRlRGF0ZUNvbHVtbih7IHR5cGU6ICd0aW1lc3RhbXAnIH0pXG4gIHB1YmxpYyB1cGRhdGVkQXQhOiBEYXRlO1xuXG4gIC8qXG4gICAqIE9uZSBUbyBNYW55IFJlbGF0aW9uc1xuICAgKi9cblxuICBAT25lVG9NYW55KCgpID0+IFVzZXJBcHAsICh1c2VyQXBwKSA9PiB1c2VyQXBwLmFwcClcbiAgcHVibGljIHVzZXJzOiBVc2VyQXBwW107XG5cbiAgLypcbiAgICogQ29uc3RydWN0b3JcbiAgICovXG5cbiAgY29uc3RydWN0b3IocGFydGlhbD86IFBhcnRpYWw8QXBwPikge1xuICAgIGlmICghcGFydGlhbCkge1xuICAgICAgcmV0dXJuO1xuICAgIH1cblxuICAgIE9iamVjdC5hc3NpZ24odGhpcywgcGFydGlhbCk7XG4gIH1cbn1cbiJdfQ==