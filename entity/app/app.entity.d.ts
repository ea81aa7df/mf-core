import { UserApp } from '../user/app.entity';
export declare class App {
    id: number;
    name: string;
    isActive: boolean;
    createdAt: Date;
    updatedAt: Date;
    users: UserApp[];
    constructor(partial?: Partial<App>);
}
//# sourceMappingURL=app.entity.d.ts.map