import { User } from '..';
export declare class Organisation {
    id: number;
    isDeleted: boolean;
    createdAt: Date;
    updatedAt: Date;
    users: User[];
}
//# sourceMappingURL=organisation.entity.d.ts.map