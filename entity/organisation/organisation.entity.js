"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Organisation = void 0;
const typeorm_1 = require("typeorm");
const __1 = require("..");
let Organisation = class Organisation {
};
__decorate([
    (0, typeorm_1.PrimaryGeneratedColumn)(),
    __metadata("design:type", Number)
], Organisation.prototype, "id", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: 'boolean', default: false }),
    __metadata("design:type", Boolean)
], Organisation.prototype, "isDeleted", void 0);
__decorate([
    (0, typeorm_1.CreateDateColumn)({ type: 'timestamp' }),
    __metadata("design:type", Date)
], Organisation.prototype, "createdAt", void 0);
__decorate([
    (0, typeorm_1.UpdateDateColumn)({ type: 'timestamp' }),
    __metadata("design:type", Date)
], Organisation.prototype, "updatedAt", void 0);
__decorate([
    (0, typeorm_1.OneToMany)(() => __1.User, (user) => user.organisation),
    __metadata("design:type", Array)
], Organisation.prototype, "users", void 0);
Organisation = __decorate([
    (0, typeorm_1.Entity)()
], Organisation);
exports.Organisation = Organisation;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoib3JnYW5pc2F0aW9uLmVudGl0eS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy9lbnRpdHkvb3JnYW5pc2F0aW9uL29yZ2FuaXNhdGlvbi5lbnRpdHkudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7O0FBQUEscUNBQWdIO0FBQ2hILDBCQUEwQjtBQUcxQixJQUFhLFlBQVksR0FBekIsTUFBYSxZQUFZO0NBdUJ4QixDQUFBO0FBckJDO0lBREMsSUFBQSxnQ0FBc0IsR0FBRTs7d0NBQ047QUFHbkI7SUFEQyxJQUFBLGdCQUFNLEVBQUMsRUFBRSxJQUFJLEVBQUUsU0FBUyxFQUFFLE9BQU8sRUFBRSxLQUFLLEVBQUUsQ0FBQzs7K0NBQ2xCO0FBTzFCO0lBREMsSUFBQSwwQkFBZ0IsRUFBQyxFQUFFLElBQUksRUFBRSxXQUFXLEVBQUUsQ0FBQzs4QkFDckIsSUFBSTsrQ0FBQztBQUd4QjtJQURDLElBQUEsMEJBQWdCLEVBQUMsRUFBRSxJQUFJLEVBQUUsV0FBVyxFQUFFLENBQUM7OEJBQ3JCLElBQUk7K0NBQUM7QUFPeEI7SUFEQyxJQUFBLG1CQUFTLEVBQUMsR0FBRyxFQUFFLENBQUMsUUFBSSxFQUFFLENBQUMsSUFBSSxFQUFFLEVBQUUsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDOzsyQ0FDOUI7QUF0QlYsWUFBWTtJQUR4QixJQUFBLGdCQUFNLEdBQUU7R0FDSSxZQUFZLENBdUJ4QjtBQXZCWSxvQ0FBWSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEVudGl0eSwgUHJpbWFyeUdlbmVyYXRlZENvbHVtbiwgQ29sdW1uLCBDcmVhdGVEYXRlQ29sdW1uLCBVcGRhdGVEYXRlQ29sdW1uLCBPbmVUb01hbnkgfSBmcm9tICd0eXBlb3JtJztcbmltcG9ydCB7IFVzZXIgfSBmcm9tICcuLic7XG5cbkBFbnRpdHkoKVxuZXhwb3J0IGNsYXNzIE9yZ2FuaXNhdGlvbiB7XG4gIEBQcmltYXJ5R2VuZXJhdGVkQ29sdW1uKClcbiAgcHVibGljIGlkITogbnVtYmVyO1xuXG4gIEBDb2x1bW4oeyB0eXBlOiAnYm9vbGVhbicsIGRlZmF1bHQ6IGZhbHNlIH0pXG4gIHB1YmxpYyBpc0RlbGV0ZWQ6IGJvb2xlYW47XG5cbiAgLypcbiAgICogQ3JlYXRlIGFuZCBVcGRhdGUgRGF0ZSBDb2x1bW5zXG4gICAqL1xuXG4gIEBDcmVhdGVEYXRlQ29sdW1uKHsgdHlwZTogJ3RpbWVzdGFtcCcgfSlcbiAgcHVibGljIGNyZWF0ZWRBdCE6IERhdGU7XG5cbiAgQFVwZGF0ZURhdGVDb2x1bW4oeyB0eXBlOiAndGltZXN0YW1wJyB9KVxuICBwdWJsaWMgdXBkYXRlZEF0ITogRGF0ZTtcblxuICAvKlxuICAgKiBPbmUgVG8gTWFueSBSZWxhdGlvbnNcbiAgICovXG5cbiAgQE9uZVRvTWFueSgoKSA9PiBVc2VyLCAodXNlcikgPT4gdXNlci5vcmdhbmlzYXRpb24pXG4gIHB1YmxpYyB1c2VyczogVXNlcltdO1xufVxuIl19