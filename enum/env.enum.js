"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Env = void 0;
var Env;
(function (Env) {
    Env["Production"] = "production";
    Env["Development"] = "development";
    Env["Staging"] = "staging";
    Env["Test"] = "test";
})(Env = exports.Env || (exports.Env = {}));
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZW52LmVudW0uanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi9zcmMvZW51bS9lbnYuZW51bS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7QUFBQSxJQUFZLEdBS1g7QUFMRCxXQUFZLEdBQUc7SUFDYixnQ0FBeUIsQ0FBQTtJQUN6QixrQ0FBMkIsQ0FBQTtJQUMzQiwwQkFBbUIsQ0FBQTtJQUNuQixvQkFBYSxDQUFBO0FBQ2YsQ0FBQyxFQUxXLEdBQUcsR0FBSCxXQUFHLEtBQUgsV0FBRyxRQUtkIiwic291cmNlc0NvbnRlbnQiOlsiZXhwb3J0IGVudW0gRW52IHtcbiAgUHJvZHVjdGlvbiA9ICdwcm9kdWN0aW9uJyxcbiAgRGV2ZWxvcG1lbnQgPSAnZGV2ZWxvcG1lbnQnLFxuICBTdGFnaW5nID0gJ3N0YWdpbmcnLFxuICBUZXN0ID0gJ3Rlc3QnLFxufVxuIl19