export declare enum Env {
    Production = "production",
    Development = "development",
    Staging = "staging",
    Test = "test"
}
//# sourceMappingURL=env.enum.d.ts.map